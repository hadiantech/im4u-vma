<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

class Nominee extends Model implements HasMedia
{
    use SoftDeletes;
    use HasMediaTrait;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    protected $fillable  = ['fullname', 'job_title', 'organisation', 'address_1', 'address_2', 'city', 'state', 'postcode', 'phone_work', 'phone_mobile','official_organisation_name', 'category_id', 'campaign_name'];

    public function category(){
        return $this->belongsTo(Category::class);
    }

}

<?php

namespace App\Http\Controllers;

use App\Nominee;
use App\Category;
use Illuminate\Http\Request;

class NomineeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $nominees = Nominee::all();
        return view('nominees.index', compact('nominees'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::pluck('name', 'id');
        return view('nominees.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'fullname' => 'required',
            'job_title' => 'required',
            'organisation' => 'required',
            'address_1' => 'required',
            'address_2' => 'required',
            'city' => 'required',
            'state' => 'required',
            'postcode' => 'required',
            'phone_work' => 'required',
            'phone_mobile' => 'required',
            'official_organisation_name' => 'required',
            'campaign_name' => 'required',
            'category_id' => 'required',
            'doc' => 'required',
        ]);

        $nominee = Nominee::create($request->all());
        $nominee->addMediaFromRequest('doc')->toMediaCollection('doc');

        $nominee->addMultipleMediaFromRequest(['supports'])
                ->each(function ($fileAdder) {
                    $fileAdder->toMediaCollection('supports');
                });

        flash('Nominee has been added');

        return redirect()->route('nominees.show', $nominee->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Nominee  $nominee
     * @return \Illuminate\Http\Response
     */
    public function show(Nominee $nominee)
    {
        return view('nominees.show', compact('nominee'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Nominee  $nominee
     * @return \Illuminate\Http\Response
     */
    public function edit(Nominee $nominee)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Nominee  $nominee
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Nominee $nominee)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Nominee  $nominee
     * @return \Illuminate\Http\Response
     */
    public function destroy(Nominee $nominee)
    {
        if( $nominee->delete() ) {
            flash()->success('Nominee has been deleted');
        } else {
            flash()->success('Nominee not deleted');
        }

        return redirect()->back();
    }

    public function term_condition(){
        return view('nominees.terms');
    }
}

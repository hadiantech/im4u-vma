<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UploadController extends Controller
{
    public function upload_image(Request $request)
    {
        $id = $request->model_id;
        $model_name = '\\App\\'.$request->model_name;
        $model = new $model_name;

        if(!$request->image){
            flash()->error('Image not found.');
            return redirect()->back();
        }

        if( $model_data = $model->findOrFail($id)) {
            $model_data->clearMediaCollection($request->collection);
            $model_data->addMediaFromRequest('image')->toMediaCollection($request->collection);
            flash('Image has been updated.');
            activity()->log('profile_image');
        } else {
            flash()->error( 'Id not found.');
        }

        return redirect()->back();
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DataTables;

use App\User;
use App\Nominee;

class DatatablesController extends Controller
{
    public function getUser(){
        return Datatables::eloquent(User::with('roles'))
        ->addColumn('action', function($model){
            $url = 'users';
            return view('shared._table_action', compact('model', 'url'))->render();
        })
        ->addColumn('roles', function($model){
            return $model->roles->map(function($roles) {
                return $roles->name;
            })->implode(', ');
        })
        ->addIndexColumn()
        ->make(true);
    }

    public function getNominee(){
        return Datatables::eloquent(Nominee::with('category'))
        ->addColumn('action', function($model){
            $url = 'nominees';
            return view('shared._table_action', compact('model', 'url'))->render();
        })
        ->addColumn('category', function($model){
            return $model->category->name;
        })
        ->addIndexColumn()
        ->make(true);
    }
}

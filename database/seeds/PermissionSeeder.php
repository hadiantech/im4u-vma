<?php

use Illuminate\Database\Seeder;
use App\Permission;
use App\Role;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Ask for db migration refresh, default is no
        if ($this->command->confirm('Do you want to remigrate permission')) {

            //truncate
            Permission::unguard();
            DB::statement('SET FOREIGN_KEY_CHECKS=0;');
            Permission::truncate();
            DB::statement('SET FOREIGN_KEY_CHECKS=1;');
            Permission::reguard();

            // Seed the default permissions
            $permissions = Permission::defaultPermissions();

            foreach ($permissions as $perms) {
                Permission::firstOrCreate(['name' => $perms]);
            }

            $role = Role::where('name', 'Admin')->first();
            $role->syncPermissions(Permission::all());
            $this->command->info('Admin granted all the permissions');
            $this->command->info('Default Permissions added.');
        }
    }
}

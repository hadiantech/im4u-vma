<?php
 
return [
  
    'home' => 'HOME',
    'about' => [
        'title' => 'ABOUT',
        'award' => 'ABOUT THE AWARDS',
        'winner' => '2016 WINNERS',
        'highlight' => 'HIGHLIGHTS',
    ],
    'sponsorship' => 'SPONSORSHIP',
    'contact' => 'CONTACT US',
    'nominate' => 'NOMINATE NOW',
 
];
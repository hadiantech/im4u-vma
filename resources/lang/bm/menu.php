<?php
 
return [
  
    'home' => 'LAMAN UTAMA',
    'about' => [
        'title' => 'TENTANG',
        'award' => 'TENTANG VMA',
        'winner' => 'PEMENANG 2016',
        'highlight' => 'SOROTAN',
    ],
    'sponsorship' => 'TAJAAN',
    'contact' => 'HUBUNGI KAMI',
    'nominate' => 'CALON SEKARANG',
 
];
@extends('layouts.basic.master')

@section('content')
<div class="container-fluid bg-black py-5 mt-3">
    <div class="container bg-black mt-5 mb-5">
        <div class="row py-">
            <div class="col-md-12 mb-5 text-center">
                <h1 class="futura text-gold">ABOUT THE AWARDS</h1>
            </div>
            <div class="col-md-6">
                <img src="img/logo/aboutpagelogo.png" class="img-fluid" style="width:90%;" />
            </div>
            <div class="col-md-6 d-flex align-items-center">
                <p class="text-white m-0">The Volunteer Malaysia Awards (VMA) recognises impactful volunteer initiatives that demonstrate positive tangible outcomes towards the social, cultural, environmental and community well-being of Malaysia. The Awards also honour, celebrate and pay tribute to those who have inspired through their commitment to volunteerism.</p>
            </div>
            <div class="col-md-12 bg-cover my-4" style="background-image: url('img/bottomphoto.jpg'); height: 450px;"></div>
            <div class="col-md-12 p-0">
                <p class="text-white pt-3 pb-3">Those who achieve the required number of volunteer service hours via iM4U’s volunteer program over the set time frame stand a chance to receive an award and certificate.</p>
            </div>
        </div>
        <div class="row p-4 bg-gray">
            <div class="col-md-4 d-flex">
                <img src="img/icon/summary/nomination%20close-01.png" style="width:auto; height:70px;" />
                <div class="d-flex flex-column justify-content-center ml-3">
                    <p class="text-white m-0 poppins bold" style="font-size:15px">15 OCTOBER 2018</p>
                    <p class="text-white m-0" style="font-size:14px">Nomination Close</p>
                </div>
            </div>
            <div class="col-md-4 d-flex">
                <img src="img/icon/summary/votestart-01.png" style="width:auto; height:70px;" />
                <div class="d-flex flex-column justify-content-center ml-3">
                    <p class="text-white m-0 poppins bold" style="font-size:15px">1 NOVEMBER 2018</p>
                    <p class="text-white m-0" style="font-size:14px">Online Voting Start</p>
                </div>
            </div>
            <div class="col-md-4 d-flex">
                <img src="img/icon/summary/awardceremony-01.png" style="width:auto; height:70px;" />
                <div class="d-flex flex-column justify-content-center ml-3">
                    <p class="text-white m-0 poppins bold" style="font-size:15px">15 NOVEMBER 2018</p>
                    <p class="text-white m-0" style="font-size:14px">Award Ceremony</p>
                </div>
            </div>
        </div>
        <div class="row mt-3 p-4 bg-gray">
            <div class="col-md-4 d-flex align-items-center">
                <p class="poppins thin text-gold m-0" style="font-size: 3.3em; line-height: 1em">24</p>
                <div class="d-flex flex-column justify-content-center ml-3">
                    <p class="text-white m-0 poppins bold" style="font-size:15px">NOVEMBER</p>
                    <p class="text-white m-0" style="font-size:14px">Saturday</p>
                </div>
            </div>
            <div class="col-md-4 d-flex align-items-center">
                <img src="img/icon/mapmarker-01.png" style="width:115px" />
                <div class="d-flex flex-column justify-content-center ml-3">
                    <p class="text-white m-0">iM4U Sentral, Jalan TPP 1/7, <br/> Taman Perindustrian Puchong, 47100 Puchong, Selangor
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
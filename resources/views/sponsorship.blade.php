@extends('layouts.basic.master')

@section('content')
<div class="container-fluid bg-black py-5 mt-3">
    <div class="container bg-black mt-5 mb-0">
        <div class="row">
            <div class="col-md-12">
                <h1 class="text-center text-gold futura">SPONSORSHIP</h1>
            </div>
        </div>
        <div class="row bg-gray mt-5">
            <div class="col-md-6 p-5 d-flex flex-column bg-cover" style="background-image: url('img/sample3.jpg');">
                <img class="align-self-center mb-4" src="img/logo/sponsorship%20logo.png" style="width: 150px" />
                <p class="medium text-gold mb-4" style="font-size:1.5em; line-height: 1.4em">So many reasons to sponsor the Volunteer Malaysia Awards 2018</p>
                <p class="text-white">(A Pepsi in a futuristic bottle appears out of the counter. Marty picks it up and looks at it.) Hey McFly! (Marty turns to Biff.) Yeah, I seen you around. You're Marty McFly's kid, aren't you? Biff? You're Marty Jr! Tough break kid, must be rough being named after a complete butthead. What's that supposed to mean?</p>
            </div>
            <div class="col-md-6 p-5">
                
                {{ Form::open(array('url' => 'contacts')) }}
                <p class="text-white mb-4" style="font-size:3em; line-height: 1em;">Contact Us</p>
                <hr class="hr-gold" />
                <div class="form-group mt-4">
                    {{ Form::text('name', null, ['class'=>"form-control", 'placeholder'=>"Name", 'required']) }}
                </div>
                <div class="form-group">
                    {{ Form::email('email', null, ['class'=>"form-control", 'placeholder'=>"Email", 'required']) }}
                </div>
                <div class="form-group">
                    {{ Form::textarea('message', null, ['class'=>"form-control", 'placeholder'=>"Message about sponsorship...", 'style'=>"height: 200px;", 'required']) }}
                </div>
                <div class="form-group text-right">
                    {{ Form::submit('SEND MESSAGE', array('class' => 'btn btn-black')) }}
                </div>

                {{ Form::close() }}

            </div>
        </div>
        <div class="row my-5 justify-content-center">
            <div class="col-md-12 text-center">
                <p class="text-white">For more information about sponsorship opportunities, please contact</p>
            </div>
            <div class="col-md-12 d-flex align-items-center justify-content-center">
                <i class="fa fa-phone text-gold m-2" style="font-size:1.5em"></i>
                <p class="text-white medium m-1" style="font-size:1.5em;">03 8064 4488</p>
            </div>
            <div class="col-md-12 d-flex align-items-center justify-content-center">
                <i class="fa fa-envelope text-gold m-2" style="font-size:1.5em"></i>
                <p class="text-white medium m-1" style="font-size:1.5em;">sponsorship@vma.my</p>
            </div>
        </div>
    </div>
</div>
@endsection
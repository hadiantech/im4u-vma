@if(session('popup'))

    {{-- Invoke from reject term --}}
    @if(session('popup') == 'reject')
        <div id="loadModal" class="modal fade" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="green-bg d-flex justify-content-center p-2" style="position:relative;">
                        <i class="far fa-thumbs-up white-text m-4" style="font-size:5em"></i>
                        <a href="" data-dismiss="modal" class="close"  aria-label="Close" style="position:absolute; top:10px; right:15px">
                            <span aria-hidden="true">&times;</span>
                        </a>
                    </div>
                    <div class="modal-body text-center">
                        <h3 class="green-text pt-3">Thank You For Creating Project!</h3>
                        <p>You have finished creating a project, it will be revised by Admin for approval. You will get notification via email after the project successfully published.</p>
                    </div>
                    <div class="modal-footer d-flex justify-content-center">
                        <a href="" data-dismiss="modal" class="btn btn-green" style="width:200px;">OK</a>
                    </div>
                </div>
            </div>
        </div>
    @endif


    {{-- Invoke from after nominee --}}
    @if(session('popup') == 'register')
        <div id="loadModal" class="modal fade" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="green-bg d-flex justify-content-center p-2" style="position:relative;">
                        <i class="far fa-thumbs-up white-text m-4" style="font-size:5em"></i>
                        <a href="" data-dismiss="modal" class="close"  aria-label="Close" style="position:absolute; top:10px; right:15px">
                            <span aria-hidden="true">&times;</span>
                        </a>
                    </div>
                    <div class="modal-body text-center">
                        <h3 class="green-text pt-3">Thank You For Creating Project!</h3>
                        <p>You have finished creating a project, it will be revised by Admin for approval. You will get notification via email after the project successfully published.</p>
                    </div>
                    <div class="modal-footer d-flex justify-content-center">
                        <a href="" data-dismiss="modal" class="btn btn-green" style="width:200px;">OK</a>
                    </div>
                </div>
            </div>
        </div>
    @endif


@endif



@section('js')
@parent 

<script type="text/javascript">
    $(window).on('load',function(){
        $('#loadModal').modal('show');
    });
</script>

@endsection
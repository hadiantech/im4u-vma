@section('css')
    @parent
@endsection

@section('js')
    @parent
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/4.4.0/bootbox.min.js"></script>

    <script>
        window.csrfToken = '<?php echo csrf_token(); ?>';
    </script>

    <script>
        (function() {

            var delete_confirm = {
                initialize: function() {
                    this.registerEvents();
                },

                registerEvents: function() {
                    $('body').on('click', 'a[data-method]', this.handleMethod);
                },

                handleMethod: function(e) {
                    e.preventDefault();
                    var link = $(this);
                    var httpMethod = link.data('method').toUpperCase();
                    var form;

                    // If the data-method attribute is not PUT or DELETE,
                    // then we don't know what to do. Just ignore.
                    if ( $.inArray(httpMethod, ['PUT', 'DELETE']) === - 1 ) {
                        return;
                    }

                    // Allow user to optionally provide data-confirm="Are you sure?"
                    // if ( link.data('confirm') ) {
                    //     if ( ! delete_confirm.verifyConfirm(link) ) {
                    //         return false;
                    //     }
                    // }
                    // form = delete_confirm.createForm(link);
                    // form.submit();
                    //
                    // e.preventDefault();

                    delete_confirm.verifyConfirm(link)
                        .done(function () {
                            form = delete_confirm.createForm(link)
                            form.submit()
                        })
                },

                verifyConfirm: function(link) {
                    // return confirm(link.data('confirm'));
                    //return confirm('Are you sure?');
                    var confirm = new $.Deferred();

                    bootbox.confirm('Are you sure?', function(result) {
                        if (result) {
                            confirm.resolve(link);
                        } else {
                            confirm.reject(link);
                        }
                    })

                    return confirm.promise()
                },

                createForm: function(link) {
                    var form =
                        $('<form>', {
                            'method': 'POST',
                            'action': link.attr('href')
                        });

                    var token =
                        $('<input>', {
                            'name': '_token',
                            'type': 'hidden',
                            'value': window.csrfToken
                        });

                    var hiddenInput =
                        $('<input>', {
                            'name': '_method',
                            'type': 'hidden',
                            'value': link.data('method')
                        });

                    return form.append(token, hiddenInput)
                        .appendTo('body');
                }
            };

            delete_confirm.initialize();

        })();
    </script>

@endsection

<!--footer kaki-->
<div class="container-fluid bg-gray">
  <div class="row">
      <div class="col-md-6 d-flex align-items-center">
          <img class="m-2 ml-5" src="/img/logo/footerlogo.png" style="height: 60px;" />
          <p class="text-white m-3" style="font-size: 12px">Copyright &copy; 2018 Volunteer Malaysia</p>
      </div>
      <div class="col-md-6 d-flex align-items-center flex-row-reverse">
          <a href="javascript:;" style="font-size: 12px" class="text-white poppins m-2 mr-5">Terms</a>
          <a href="javascript:;" style="font-size: 12px" class="text-white poppins m-2">Privacy Policy</a>
      </div>
  </div>

</div>
<!-- Navigation -->
<div class="container-fluid bg-black" style="z-index: 10; font-size: 14px">
  <div class="row">
      <nav class="navbar navbar-expand-lg navbar-dark" style="width: 100%">
          <a class="navbar-brand" href="{{route('home')}}">
              <img src="/img/logo/topmenulogo.png" width="140" height="auto" class="d-inline-block align-top" alt="">
          </a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse navbarfix" id="navbarNavAltMarkup">
              <div class="navbar-nav">
                    
                  <a class="poppins medium nav-item nav-link" href="{{route('home')}}">@lang('menu.home') <span class="sr-only">(current)</span></a>
                  <div class="sub" style="position:relative;">
                      <a class="poppins medium nav-item nav-link" href="{{route('about')}}">@lang('menu.about.title')</a>
                      <div class="navbar-sub" style="position: absolute; left:0px;">
                          <a class="poppins navbar-sub-item" href="{{route('about')}}">@lang('menu.about.award')</a>
                          <a class="poppins navbar-sub-item" href="{{route('listcategories')}}">LIST OF CATEGORIES</a>
                          <a class="poppins navbar-sub-item" href="{{route('winners')}}">@lang('menu.about.winner')</a>
                          <a class="poppins navbar-sub-item" href="{{route('highlights')}}">@lang('menu.about.highlight')</a>
                      </div>
                  </div>
                  <a class="poppins medium nav-item nav-link" href="{{route('sponsorship')}}">@lang('menu.sponsorship')</a>
                  <a class="poppins medium nav-item nav-link" href="{{route('contact')}}">@lang('menu.contact')</a>
                  <a class="poppins medium nav-item nav-link special" href="{{route('nominees.terms')}}">@lang('menu.nominate')</a>
                  <div class="d-flex align-items-center pl-1 pr-4">
                        <a class="poppins small nav-item-language nav-link mx-1-0 p-0" href="{{route('locale', 'en')}}">EN</a>
                        <a class="poppins small nav-item-language nav-link mx-1 p-0" href="{{route('locale', 'bm')}}">BM</a>
                  </div>


                  @guest
                  
                  @else
                    <div class="sub" style="position:relative;">
                        <a class="poppins medium nav-item nav-link">{{ Auth::user()->name }}</a>
                        <div class="navbar-sub" style="position: absolute; left:0px;">
                            <a class="poppins navbar-sub-item" href="{{route('dashboard')}}">Dashboard</a>
                            <a class="poppins navbar-sub-item" href="{{route('profile')}}">My Profile</a>
                            <a class="poppins navbar-sub-item" href="{{route('logout')}}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Logout</a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </div>
                    </div>
                  @endguest
              </div>
              
          </div>
      </nav>
  </div>
</div>
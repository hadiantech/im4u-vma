@extends('layouts.dashboard.master')

@section('title', 'Nominee Management')

@section('content')

<div class="card">
    <div class="card-body">
        @can('add_nominees')
            <a href="{{route('nominees.create')}}" class="btn btn-success">
                <i class="fas fa-plus"></i> New
            </a>
        @endcan
    </div>
</div>

<br />
<table id="nominees-table" class="table table-bordered">
    <thead>
        <tr>
            <th>#</th>
            <th>Campaign/ Initiative/ Programme Name</th>
            <th>Category</th>
            <th>Created At</th>
            <th>Action</th>
        </tr>
    </thead>
</table>

@endsection

@section('js')
@parent

<script>
    $(function () {
        $('#nominees-table').DataTable({
            serverSide: true,
            processing: true,
            ajax: '/datatables/getNominee',
            columns: [
                {data: 'DT_Row_Index', orderable: false, searchable: false},
                {data: 'campaign_name'},
                {data: 'category', name: 'category.name', orderable: false},
                {data: 'created_at'},
                {data: 'action', orderable: false, searchable: false}
            ]
        });
    });
</script>

@endsection
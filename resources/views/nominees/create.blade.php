@extends('layouts.basic.master')

@section('content')
<div class="container-fluid bg-black py-5 bg-cover" style="background-image: url('/img/sample3.jpg');">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-md-6">
                <img class="img-fluid" src="/img/logo/aboutpagelogo.png" style="width:400px;" />
            </div>
            <div class="col-md-6">
                <h2 class="text-white futura m-1 pl-5">NOMINATION SUBMISSION</h2>
                <p class="text-white m-1 mt-2 pl-5" style="font-size: 1.3em;">Nomination close on 30 August 2018</p>
            </div>
        </div>
    </div>
</div>
{!! Form::open(['url' => route('nominees.store'), 'files' => true]) !!}
<div class="container-fluid bg-white2">
    <div class="container">
        <div class="row p-5">
            <div class="col-md-12">
                <ul class="nav nav-pills nav-fill mb-3" id="pills-tab" role="tablist">
                    <li class="nav-item current">
                        <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true">Personal Information</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false">Nomination Submission</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-contact" role="tab" aria-controls="pills-contact" aria-selected="false">Confirmation</a>
                    </li>
                </ul>
                <div class="tab-content" id="pills-tabContent">
                    <div class="tab-pane fade show active " id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                        <div class="row bg-white m-0">
                            <div class="col-md-12 pr-4 pt-3 pb-3">
                                <div class="form-inline">
                                    <p class="m-4 col-md-5">Full Name (as per IC) <span style="color:red">*</span></p>
                                    {{ Form::text('fullname', null, ['id'=> 'name', 'onchange'=>'setvalue()', 'class'=> 'form-control nominateform m-3 col-md-6', 'required', 'placeholder' => ''] ) }}
                                </div>
                                <div class="form-inline">
                                    <p class="m-4 col-md-5">Job Title <span style="color:red">*</span></p>
                                    {{ Form::text('job_title', null, ['id'=> 'job', 'onchange'=>'setvalue()','class'=> 'form-control nominateform m-3 col-md-6', 'required', 'placeholder' => ''] ) }}
                                </div>
                                <div class="form-inline">
                                    <p class="m-4 col-md-5">Organisation/Company <span style="color:red">*</span></p>
                                    {{ Form::text('organisation', null, ['id'=> 'org', 'onchange'=>'setvalue()', 'class'=> 'form-control nominateform m-3 col-md-6', 'required', 'placeholder' => ''] ) }}
                                </div>
                                <div class="form-inline">
                                    <p class="m-4 col-md-5">Address 1 <span style="color:red">*</span></p>
                                    {{ Form::text('address_1', null, ['id'=> 'address1', 'onchange'=>'setvalue()', 'class'=> 'form-control nominateform m-3 col-md-6', 'required', 'placeholder' => ''] ) }}
                                </div>
                                <div class="form-inline">
                                    <p class="m-4 col-md-5">Address 2 <span style="color:red">*</span></p>
                                    {{ Form::text('address_2', null, ['id'=> 'address2', 'onchange'=>'setvalue()', 'class'=> 'form-control nominateform m-3 col-md-6', 'required', 'placeholder' => ''] ) }}
                                </div>
                                <div class="form-inline">
                                    <p class="m-4 col-md-5">Town/City <span style="color:red">*</span></p>
                                    {{ Form::text('city', null, ['id'=> 'city', 'onchange'=>'setvalue()', 'class'=> 'form-control nominateform m-3 col-md-6', 'required', 'placeholder' => ''] ) }}
                                </div>
                                <div class="form-inline">
                                    <p class="m-4 col-md-5">State <span style="color:red">*</span></p>
                                    {{ Form::text('state', null, ['id'=> 'state', 'onchange'=>'setvalue()', 'class'=> 'form-control nominateform m-3 col-md-6', 'required', 'placeholder' => ''] ) }}
                                </div>
                                <div class="form-inline">
                                    <p class="m-4 col-md-5">Postcode <span style="color:red">*</span></p>
                                    {{ Form::text('postcode', null, ['id'=> 'postcode', 'onchange'=>'setvalue()', 'class'=> 'form-control nominateform m-3 col-md-6', 'required', 'placeholder' => ''] ) }}
                                </div>
                                <div class="form-inline">
                                    <p class="m-4 col-md-5">Work Telephone <span style="color:red">*</span></p>
                                    {{ Form::text('phone_work', null, ['id'=> 'phone', 'onchange'=>'setvalue()', 'class'=> 'form-control nominateform m-3 col-md-6', 'required', 'placeholder' => ''] ) }}
                                </div>
                                <div class="form-inline">
                                    <p class="m-4 col-md-5">Mobile Phone <span style="color:red">*</span></p>
                                    {{ Form::text('phone_mobile', null, ['id'=> 'mobile', 'onchange'=>'setvalue()', 'class'=> 'form-control nominateform m-3 col-md-6', 'required', 'placeholder' => ''] ) }}
                                </div>
                            </div>
                        </div>
                        <br/>
                        <div class="row bg-white m-0">
                            <div class="col-md-12">
                                <div class="form-inline">
                                    <p class="m-4 col-md-5">Company/Organisation's Name (in written forms, table, signage, trophy etc) <span style="color:red">*</span></p>
                                    {{ Form::text('official_organisation_name', null, ['id'=> 'org2', 'onchange'=>'setvalue()', 'class'=> 'form-control nominateform m-3 col-md-6', 'required', 'placeholder' => ''] ) }}
                                </div>
                            </div>
                        </div>
                        <br/>
                        <div class="row">
                            <div class="col-md-12 text-right">
                                <a href="javascript:;" id="gototab2" class="btn btn-black btn-w">Next</a>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
                       <div class="row bg-white m-0">
                            <div class="col-md-12 pr-4 pt-3 pb-3">
                                
                                <div class="form-inline">
                                    <p class="m-4 col-md-5">Campaign/ Initiative/ Programme Name <span style="color:red">*</span></p>
                                    {{ Form::text('campaign_name', null, ['id'=> 'mobile', 'onchange'=>'setvalue()', 'class'=> 'form-control nominateform m-3 col-md-6', 'required', 'placeholder' => ''] ) }}
                                </div>

                                <div class="form-inline">
                                    <p class="m-4 col-md-5">Category <span style="color:red">*</span></p>
                                    {{ Form::select('category_id', $categories, null, ['id'=> 'category_id', 'onchange'=>'setvalue()', 'class'=> 'form-control nominateform m-3 col-md-6', 'required', 'placeholder' => ''] ) }}
                                </div>

                                <div class="col-md-12 m-4">
                                    <p style="font-size:18px; font-weight:semibold;">Please follow the instructions below for the nomination form: </p>
                                </div>

                                <div class="form-inline">
                                    <p class="m-4 col-md-5">1.&nbsp;&nbsp;Download Nomination Form</p>
                                    <a href="{{ asset('file/VMANominationForm.pdf') }}" download="VMANominationForm.pdf" target="_blank" class="btm-default m-4 col-md-5">Download Here</a>
                                </div>
                                <div class="form-inline">
                                    <p class="m-4 col-md-5">2.&nbsp;&nbsp;Upload Completed Nomination Form<span style="color:red">*</span></p>
                                    <input name="doc" id="doc" onchange="setvalue();" type="file" class="form-control nominateform m-3 col-md-6">
                                </div>

                                <hr />
                                <div class="form">
                                    <p class="m-4 col-md-5">Support Documents</p>
                                    {{Form::file('supports[]', ['class' => 'form-control nominateform m-3 col-md-6'])}}
                                    {{Form::file('supports[]', ['class' => 'form-control nominateform m-3 col-md-6'])}}
                                    {{Form::file('supports[]', ['class' => 'form-control nominateform m-3 col-md-6'])}}
                                    {{Form::file('supports[]', ['class' => 'form-control nominateform m-3 col-md-6'])}}
                                    {{Form::file('supports[]', ['class' => 'form-control nominateform m-3 col-md-6'])}}
                                    {{Form::file('supports[]', ['class' => 'form-control nominateform m-3 col-md-6'])}}
                                    {{Form::file('supports[]', ['class' => 'form-control nominateform m-3 col-md-6'])}}
                                    {{Form::file('supports[]', ['class' => 'form-control nominateform m-3 col-md-6'])}}
                                    {{Form::file('supports[]', ['class' => 'form-control nominateform m-3 col-md-6'])}}
                                    {{Form::file('supports[]', ['class' => 'form-control nominateform m-3 col-md-6'])}}
                                </div>
                           </div>
                        </div>
                        <br/>
                        <div class="row">
                            <div class="col-md-6">
                                <a href="javascript:;" id="gototab1" class="btn btn-black btn-w">Back</a>
                               
                            </div>
                            <div class="col-md-6 text-right">
                                 <a href="javascript:;" id="gototab3" class="btn btn-black btn-w">Next</a>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="pills-contact" role="tabpanel" aria-labelledby="pills-contact-tab">
                        <div class="row bg-white m-0">
                            <div class="col-md-12 pr-4 pt-3 pb-3">
                                <div class="col-md-12 text-center">
                                    <p class="m-4" style="font-size:1.1em">Please confirm your information below. To change the information, you can click on the tabs above. <span class="bold">You will be unable to submit if any of the fields incomplete!</span></p>
                                </div>
                                <div class="form-inline">
                                    <p class="m-4 col-md-5">Full Name (as per IC) <span style="color:red">*</span></p>
                                    <input id="nameC" type="text" class="form-control nominateform m-3 col-md-6 disabled" disabled>
                                </div>
                                <div class="form-inline">
                                    <p class="m-4 col-md-5">Job Title <span style="color:red">*</span></p>
                                    <input id="jobC" type="text" class="form-control nominateform m-3 col-md-6 disabled" disabled>
                                </div>
                                <div class="form-inline">
                                    <p class="m-4 col-md-5">Organisation/Company <span style="color:red">*</span></p>
                                    <input id="orgC" type="text" class="form-control nominateform m-3 col-md-6 disabled" disabled>
                                </div>
                                <div class="form-inline">
                                    <p class="m-4 col-md-5">Address 1 <span style="color:red">*</span></p>
                                    <input id="address1C" type="text" class="form-control nominateform m-3 col-md-6 disabled" disabled>
                                </div>
                                <div class="form-inline">
                                    <p class="m-4 col-md-5">Address 2 <span style="color:red">*</span></p>
                                    <input id="address2C" type="text" class="form-control nominateform m-3 col-md-6 disabled" disabled>
                                </div>
                                <div class="form-inline">
                                    <p class="m-4 col-md-5">Town/City <span style="color:red">*</span></p>
                                    <input id="cityC" type="text" class="form-control nominateform m-3 col-md-6 disabled" disabled>
                                </div>
                                <div class="form-inline">
                                    <p class="m-4 col-md-5">State <span style="color:red">*</span></p>
                                    <input id="stateC" type="text" class="form-control nominateform m-3 col-md-6 disabled" disabled>
                                </div>
                                <div class="form-inline">
                                    <p class="m-4 col-md-5">Postcode <span style="color:red">*</span></p>
                                    <input id="postcodeC" type="text" class="form-control nominateform m-3 col-md-6 disabled" disabled>
                                </div>
                                <div class="form-inline">
                                    <p class="m-4 col-md-5">Work Telephone <span style="color:red">*</span></p>
                                    <input id="phoneC" type="text" class="form-control nominateform m-3 col-md-6 disabled" disabled>
                                </div>
                                <div class="form-inline">
                                    <p class="m-4 col-md-5">Mobile Phone <span style="color:red">*</span></p>
                                    <input id="mobileC" type="text" class="form-control nominateform m-3 col-md-6 disabled" disabled>
                                </div>
                            </div>
                        </div>
                        <br/>
                        <div class="row bg-white m-0">
                            <div class="col-md-12">
                                <div class="form-inline">
                                    <p class="m-4 col-md-5">Company/Organisation's Name (in written forms, table, signage, trophy etc) <span style="color:red">*</span></p>
                                    <input id="org2C" onchange="setvalue();" type="text" class="form-control nominateform m-3 col-md-6" disabled>
                                </div>
                            </div>
                        </div>
                        <br/>
                        <div class="row bg-white m-0">
                            <div class="col-md-12">
                                <div class="form-inline">
                                    <p class="m-4 col-md-5">Uploaded Document <span style="color:red">*</span></p>
                                    <p id="docC"> Here</p>
                                </div>
                            </div>
                        </div>
                        <br/>
                        <div class="row">
                            <div class="col-md-6 col-xs-12">
                                <a href="javascript:;" id="gototab2-2" class="btn btn-black d-none d-sm-block">Back</a>
                                <a href="javascript:;" id="gototab2-2" class="btn btn-black btn-block d-block d-sm-none">Back</a>
                            </div>
                            <div class="col-md-6 col-xs-12 text-right mt-1">
                                {{ Form::submit('Submit Nomination', ['class'=> 'btn btn-black d-none d-sm-block'] ) }}
                                {{ Form::submit('Submit Nomination', ['class'=> 'btn btn-black btn-block d-block d-sm-none'] ) }}
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection

@section('js') 
@parent
<script>
    $('.nav-pills .nav-item').click(function() {
        $('.current').removeClass('current');
        $(this).addClass('current');
    });
    
    $('#gototab1').on('click', function (e) {
        e.preventDefault()
        $('#pills-tab li:nth-child(1) a').tab('show');
        $('.current').removeClass('current');
        $('#pills-tab li:nth-child(1)').addClass('current');
    })
    
    $('#gototab2').on('click', function (e) {
        e.preventDefault()
        $('#pills-tab li:nth-child(2) a').tab('show');
        $('.current').removeClass('current');
        $('#pills-tab li:nth-child(2)').addClass('current');
    })

     $('#gototab2-2').on('click', function (e) {
        e.preventDefault()
        $('#pills-tab li:nth-child(2) a').tab('show');
        $('.current').removeClass('current');
        $('#pills-tab li:nth-child(2)').addClass('current');
    })
    
    $('#gototab3').on('click', function (e) {
        e.preventDefault()
        $('#pills-tab li:nth-child(3) a').tab('show');
        $('.current').removeClass('current');
        $('#pills-tab li:nth-child(3)').addClass('current');
    })
</script>

<script>
    function setvalue(){
        $('#nameC').val($('#name').val());
        $('#jobC').val($('#job').val());
        $('#orgC').val($('#org').val());
        $('#address1C').val($('#address1').val());
        $('#address2C').val($('#address2').val());
        $('#cityC').val($('#city').val());
        $('#stateC').val($('#state').val());
        $('#postcodeC').val($('#postcode').val());
        $('#phoneC').val($('#phone').val());
        $('#nameC').val($('#name').val());
        $('#mobileC').val($('#mobile').val());
        $('#org2C').val($('#org2').val());
        
        var filename = $('#doc').val();
        if (filename.substring(3,11) == 'fakepath') {
            filename = filename.substring(12);
        } // Remove c:\fake at beginning from localhost chrome
        $('#docC').text(filename);
        
        
    }
    
</script>
@endsection
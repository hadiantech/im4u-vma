@extends('layouts.basic.master')

@section('content')
<div class="container p-5 bg-black">
    <h1 class="text-white futura uppercase">{{ $nominee->campaign_name }} </h1>
    <h2 class="text-white">Category: {{ $nominee->category->name }} </h2>
    
    <p class="text-white bold">Form </p>
    <a target="_blank" href="{{ $nominee->getMedia('doc')->first()->getUrl() }}" class="btn btn-sm btn-white-outline"><i class="fas fa-file-download mr-3"></i> {{ $nominee->getMedia('doc')->first()->name }}</a>
    
    <br />
    <br />
    <p class="text-white bold">Support File</p>
    @forelse($nominee->getMedia('supports') as $support)
    <a target="_blank" href="{{ $support->getUrl() }}" class="btn btn-sm btn-white-outline"><i class="fas fa-file-download mr-3"></i> {{ $support->name }}</a>
    <br />
    @empty
    <p class="text-white">No Support Document</p>
    @endforelse
    <hr class="hr-gold" />
    
    <p class="text-white bold">SUBMITTED BY</p>
    <p class="text-white">{{ $nominee->fullname }} </p>
    <p class="text-white">{{ $nominee->job_title }}, {{ $nominee->organisation }}  </p>
    <p class="text-white">{{ $nominee->official_organisation_name }} </p>

    <p class="text-white bold">PHONE</p>
    <p class="text-white">{{ $nominee->phone_work }} (work) </p>
    <p class="text-white mb-5">{{ $nominee->phone_mobile }} (mobile) </p>

    <p class="text-white bold">ADDRESS</p>
    <p class="text-white">
        {{ $nominee->address_1 }}<br/>
        {{ $nominee->address_2 }}<br/>
        {{ $nominee->city }},  {{ $nominee->postcode }}<br/>
        {{ $nominee->state }}
    </p>

    
    
</div>


@endsection
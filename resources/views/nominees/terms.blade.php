@extends('layouts.basic.master')

@section('content')
<div class="container-fluid bg-black py-5 bg-cover" style="background-image: url('/img/sample3.jpg');">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-md-6">
                <img class="img-fluid" src="/img/logo/aboutpagelogo.png"  style="width:400px;" />
            </div>
            <div class="col-md-6">
                <h2 class="text-white futura m-1 pl-5">TERMS AND CONDITIONS</h2>
                <p class="text-white m-1 mt-2 pl-5" style="font-size: 1.3em;">Please read before proceed to nomination</p>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid bg-white">
    <div class="container">
        <div class="row p-5">
            <div class="col-md-12 text-center">
                <h1 class="poppins bold">Terms & Conditions</h1>
                <p class="">This is a legal agreement between you and VMA secretariat. Please read carefully before proceed.</p>
            </div>
            <div class="col-md-12">
                <div class="border p-4" style="height: 500px; overflow-y: scroll;">
                    <p>They used her thumbprint to assess her ID. Since her thumbprint never changes over the years they simply assume she's the Jennifer of the future. Well, we gotta stop them! What are we going to say, that we're time travelers? Hey, they'd have us committed. (Alley) (Meanwhile the officers have been performing checks on Jennifer.)</p>
                    <p>Yeah, 70 years, 2 months and 12 days to be exact. Here, sign on line 6 please, here you are. (He gives Marty a clipboard and Marty signs on line 6. He reads the bottom of the letter - it says "'Doc' Emmett L Brown, September 1st 1885".) It's from the Doc! (reading) "Dear Marty, if my calculations are correct you will receive this letter immediately after you saw the DeLorean struck by lightning. First let me assure you that I am alive and well. I've been living happily these past eight months in the year 1885. The lightning bolt-" 1885?! (at bottom of letter) "September 1885!" Argh! No wait kid, wait a minute, what's this all about? He's alive! The Doc's alive! He's in the old west but he's alive!</p>
                    <p>The risk is just too great as this incident proves. (He opens the door and they put Jennifer inside.) And I was behaving responsibly! Just imagine the danger if the time machine were to fall in the wrong hands! (Cut to Biff. He groans again and falls over. Cut back to Doc.) My only regret is that I will never get a chance to visit my favourite historical era - the old west. But time travelling is just too dangerous. Better that I devote myself to study the other great mystery of the universe - women. (Cut to inside the DeLorean. Marty is sitting in the passenger seat with Jennifer, still out cold, on his lap. Einstein is behind them. Doc inputs October 26th 1985 9.00PM in the time panel.) Marty, Einie, brace yourselves for temporal displacement.</p>
                    <p>I haven't. You OK, is everything all right? (Marty looks at the front door of the house where George and Lorraine are smiling as they watch him and Jennifer. Then they turn and go inside the house.) Aw yeah, everything is great. (Marty leans in to kiss Jennifer when three sonic booms are heard. They look up and the DeLorean comes flying up the driveway. It knocks over a few trash cans in the process. Doc gets out of the car wearing futuristic clothing.)</p>
                    <p>(laughs) No, this is for your coffin. My coffin?! Well, the odds are running two to one against you. Might as well be prepared. (he walks off) So...it may not be my name that's supposed to end up on that tombstone. It may be yours. (clearly shocked) Great Scott!</p>
                    <p>What do you mean you've seen this, it's brand new. Yeah well, I saw it on a rerun. What's a rerun? You'll find out. You know Marty, you look so familiar, do I know your mother?</p>
                </div>
            </div>
            <div class="col-md-12 text-center p-2 mt-4">
                <i class="fa fa-copy m-2"></i>
                <a href="javascript:;" class="m-2">View the agreement as PDF</a>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 text-center mb-5">
                <a href="{{ route('home') }}" class="btn btn-white m-3 btn-w">DECLINE</a>
                <a href="{{ route('nominees.create') }}" class="btn btn-black m-2 btn-w">ACCEPT</a>
            </div>
        </div>
    </div>
</div>

@endsection
@extends('layouts.basic.master')

@section('content')
<div class="container-fluid bg-black py-5 mt-3 pb-5">
    <div class="container bg-black mt-5"  style="margin-bottom:270px;">
        <div class="row">
            <div class="col-md-12">
                <h1 class="text-center text-gold futura">CONTACT US</h1>
            </div>
        </div>
        <div class="row mt-5">
            <div class="col-md-4">
                <div class="bg-gray p-5 pt-4">
                    <p class="text-gold bold mb-4" style="font-size:1.7em; line-height: 1.2em;">GENERAL ENQUIRIES</p>
                    <!-- <p class="text-white bold m-0">Jane Doe</p>
                    <p class="text-white m-0">PR Executive</p> -->
                    <div class="d-flex align-items-center">
                        <i class="fa fa-phone text-gold mr-2" style="width: 20px"></i>
                        <p class="text-white m-2">03 8064 4488</p>
                    </div>
                    <!-- <div class="d-flex align-items-center">
                        <i class="fa fa-mobile text-gold mr-2" style="width: 20px"></i>
                        <p class="text-white m-2">+60 1234 5678</p>
                    </div> -->
                    <div class="d-flex align-items-center">
                        <i class="fas fa-envelope text-gold mr-2" style="width: 20px"></i>
                        <p class="text-white m-2">hello@vma.my</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="bg-gray p-5 pt-4">
                    <p class="text-gold bold mb-4" style="font-size:1.7em; line-height: 1.2em;">SPONSORSHIP OPPORTUNITIES</p>
                    <!-- <p class="text-white bold m-0">Tony Stark</p>
                    <p class="text-white m-0">PR Executive</p> -->
                    <div class="d-flex align-items-center">
                        <i class="fa fa-phone text-gold mr-2" style="width: 20px"></i>
                        <p class="text-white m-2">03 8064 4488</p>
                    </div>
                    <!-- <div class="d-flex align-items-center">
                        <i class="fa fa-mobile text-gold mr-2" style="width: 20px"></i>
                        <p class="text-white m-2">+60 1234 5678</p>
                    </div> -->
                    <div class="d-flex align-items-center">
                        <i class="fas fa-envelope text-gold mr-2" style="width: 20px"></i>
                        <p class="text-white m-2">sponsorship@vma.my</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="bg-gray p-5 pt-4">
                    <p class="text-gold bold mb-4" style="font-size:1.7em; line-height: 1.2em;">MEDIA</br>ENQUIRIES</p>
                    <!-- <p class="text-white bold m-0">Yukko</p>
                    <p class="text-white m-0">PR Executive</p> -->
                    <div class="d-flex align-items-center">
                        <i class="fa fa-phone text-gold mr-2" style="width: 20px"></i>
                        <p class="text-white m-2">03 8064 4488</p>
                    </div>
                    <!-- <div class="d-flex align-items-center">
                        <i class="fa fa-mobile text-gold mr-2" style="width: 20px"></i>
                        <p class="text-white m-2">+60 1234 5678</p>
                    </div> -->
                    <div class="d-flex align-items-center">
                        <i class="fas fa-envelope text-gold mr-2" style="width: 20px"></i>
                        <p class="text-white m-2">pr@vma.my</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js') 
@parent
@endsection
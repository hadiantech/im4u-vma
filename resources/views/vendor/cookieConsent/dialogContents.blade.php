<div class="js-cookie-consent cookie-consent alert alert-warning fixed-bottom">

    <span class="cookie-consent__message">
        {!! trans('cookieConsent::texts.message') !!}
    </span>

    <button class="js-cookie-consent-agree cookie-consent__agree btn btn-warning">
        {{ trans('cookieConsent::texts.agree') }}
    </button>

</div>

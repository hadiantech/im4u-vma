@extends('layouts.dashboard.master')

@section('title', 'Category Management')

@section('content')

<div class="card">
    <div class="card-body">
        @can('add_categories')
            <a href="{{route('categories.create')}}" class="btn btn-success">
                <i class="fas fa-plus"></i> New
            </a>
        @endcan
    </div>
</div>

<br />
<table id="users-table" class="table table-bordered">
    <thead>
        <tr>
            <th>#</th>
            <th>Name</th>
            <th>Created At</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
        @foreach($categories as $category)
        <tr>
            <td>{{ $loop->iteration }}</td>
            <td>{{ $category->name }}</td>
            <td>{{ $category->created_at }}</td>
            <td>
                @include('shared._table_action', ['model' => $category, 'url' => 'categories'])
            </td>
        </tr>
        @endforeach
    </tbody>
</table>

@endsection
@extends('layouts.dashboard.master')

@section('title', 'User Management')
@section('subtitle', 'Create')

@section('content')

<div class="row">
    <div class='col-lg-12'>

    {{ Form::open(array('url' => 'categories')) }}

    <div class="form-group">
        {{ Form::label('name', 'Name') }}
        {{ Form::text('name', null, array('class' => 'form-control', 'required')) }}
    </div>

    <div class="form-group">
        {{ Form::label('description', 'Description') }}
        {{ Form::textarea('description', null, array('class' => 'form-control')) }}
    </div>

    <a class="btn btn-link" href="{{ route('categories.index') }}">Back</a>

    {{ Form::submit('Add', array('class' => 'btn btn-primary btn-tools btn-xxx')) }}

    {{ Form::close() }}

    </div>
</div>

@endsection
@extends('layouts.dashboard.master')

@section('title', 'My Profile')
@section('subtitle', 'Edit Password')

@section('content')

<div class="row">
    <div class='col-lg-12'>

    {{ Form::model($user, ['route' => ['profile.pw_update'], 'method' => 'PUT']) }}

    <div class="form-group">
        {{ Form::label('old_password', 'Old Password') }}
        {{ Form::password('old_password', array('class' => 'form-control', 'required')) }}
    </div>

    <div class="form-group">
        {{ Form::label('password', 'Password') }}
        {{ Form::password('password', array('class' => 'form-control', 'required')) }}
    </div>

    <div class="form-group">
        {{ Form::label('password_confirmation', 'Confirm Password') }}
        {{ Form::password('password_confirmation', array('class' => 'form-control', 'required')) }}
    </div>

    <a class="btn btn-link" href="{{ url()->previous() }}">Back</a>
    {{ Form::submit('Update', array('class' => 'btn btn-primary btn-tools btn-xxx')) }}

    {{ Form::close() }}

    </div>
</div>

@endsection
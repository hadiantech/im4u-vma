@extends('layouts.dashboard.master')

@section('title', 'My Profile')
@section('subtitle', '')

@section('content')

<div class="row">
    <div class='col-lg-12'>

    <table class="table table-bordered">
        <tr>
            <td width="300px" rowspan="4">
                    <div class="card" style="width: 18rem;">
                        @if(!$user->getMedia('profile')->first())
                        <img class="card-img-top" src="http://via.placeholder.com/300x300">
                        @else

                        <img class="card-img-top" src="{{ $user->getMedia('profile')->first()->getUrl()}} ">
                        @endif

                        <div class="card-body">
                            @include('shared._upload_image', ['collection' => 'profile', 'model_name' => 'User', 'model_id' => $user->id])
                        </div>
                    </div>
            </td>
            <td><b>Name</b></td>
            <td>{{$user->name}}</td>
        </tr>
        <tr>
            <td><b>Email</b></td>
            <td>{{$user->email}}</td>
        </tr>
        <tr>
            <td><b>Roles</b></td>
            <td>
                @foreach($user->roles as$role)
                    {{$role->name}} <br>
                @endforeach
            </td>
        </tr>
    </table>

    <a class="btn btn-info" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">View Activity Log</a>
    <a class="btn btn-warning" href="{{route('profile.edit')}}">Edit</a>
    <a class="btn btn-warning" href="{{route('profile.edit_pw')}}">Change Password</a>

    </div>
</div>

<div class="row collapse" id="collapseExample">
        <div class='col-lg-12'>
            <hr />
            <h3>Log Activity</h3>
            <table class="table table-bordered">
                    <tr>
                        <td>Time</td>
                        <td>Model Changes</td>
                        <td>Description</td>
                        <td>Model ID</td>
                    </tr>
                    @foreach($logs as $log)
                    <tr>
                        <td>
                            {{$log->created_at}}
                        </td>
                        <td>
                            {{$log->subject_type}}
                        </td>
                        <td>
                            {{$log->description}}
                        </td>
                        <td>
                            {{$log->subject_id}}
                        </td>
                    </tr>
                    @endforeach
                </table>
        </div>
</div>

@endsection
@extends('layouts.basic.master')

@section('content')
<div class="container-fluid p-0 m-0" style="z-index: 1; position: relative;">
  <div class="video p-0 m-0">
      <video autoplay muted loop id="myVideo" class="p-0 m-0" style="width:100%; opacity:0.5;">
          <source src="img/iM4U Volunteer Malaysia Awards 2016Trim.mp4" type="video/mp4">
          <source src="img/iM4U Volunteer Malaysia Awards 2016Trim.ogv" type="video/ogv">
          <source src="img/iM4U Volunteer Malaysia Awards 2016Trim.webm" type="video/webm">
      </video>
  </div>
  <div class="videotext row align-items-center justify-content-center m-0">
      <div class="col-md-12 p-3 text-center">
          <img src="img/logo/sliderlogo.png" class="img-fluid mt-0" style="max-width:380px;" />
          <h2 class="futura text-white mt-0" style="shadow: 2px 2px 2px 2px #191919;">DO YOUR VOLUNTEER INITIATIVE MADE A GREAT IMPACT?</h2>
          <p class="poppins text-white" style="shadow: 2px 2px 2px 2px #191919;">Know of an outstanding volunteer who deserves recognition? Nominate them!</p>
          <a href="{{ route('nominees.terms') }}" class="btn btn-black mt-1 shadow-lg">NOMINATE NOW</a>
      </div>
  </div>
</div>
<!--  Summary-->
<div class="container-fluid bg-black p-0 summary">
  <div class="container p-5">
      <div class="row align-items-stretch" style="margin-top:45px; margin-bottom:45px;">
          <div class="col-md-9">
              <div class="row">
                  <div class="col-md-12">
                      <h2 class="futura text-gold mb-5">WHAT IS VOLUNTEER MALAYSIA AWARDS</h2>
                      <p class="text-white mr-5">The Volunteer Malaysia Awards (VMA) recognises impactful volunteer initiatives that demonstrate positive tangible outcomes towards the social, cultural, environmental and community well-being of Malaysia. The Awards also honour, celebrate and pay tribute to those who have inspired through their commitment to volunteerism.</br></br></br>Checkout the info below about important dates for Volunteer Malaysia Awards 2018
                      </p>
                  </div>
                  <div class="col-md-12">
                      <hr class="hr-gold mr-5 mt-2" />
                      <div class="row">
                          <div class="col-md-4 d-flex">
                              <img src="img/icon/summary/nomination%20close-01.png" style="width:auto; height:70px;" />
                              <div class="d-flex flex-column justify-content-center">
                                  <p class="text-white m-0 poppins bold" style="font-size:13px">15 OCTOBER 2018</p>
                                  <p class="text-white m-0" style="font-size:13px">Nomination Close</p>
                              </div>
                          </div>
                          <div class="col-md-4 d-flex">
                              <img src="img/icon/summary/votestart-01.png" style="width:auto; height:70px;" />
                              <div class="d-flex flex-column justify-content-center">
                                  <p class="text-white m-0 poppins bold" style="font-size:13px">1 NOVEMBER 2018</p>
                                  <p class="text-white m-0" style="font-size:13px">Online Voting Start</p>
                              </div>
                          </div>
                          <div class="col-md-4 d-flex">
                              <img src="img/icon/summary/awardceremony-01.png" style="width:auto; height:70px;" />
                              <div class="d-flex flex-column justify-content-center">
                                  <p class="text-white m-0 poppins bold" style="font-size:13px">24 NOVEMBER 2018</p>
                                  <p class="text-white m-0" style="font-size:13px">Award Ceremony</p>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
          <div class="col-md-3 bg-cover" style="background-image:url('img/vmatrophy.JPG'); height:370px;">
          </div>
      </div>
  </div>

</div>

<!--    Award Ceremony-->
<div class="container-fluid bg-black pt-0 pl-0 pr-0 pb-5 my-3">
  <div class="container p-5">
      <div class="row mt-5 mb-5">
          <div class="col-md-12 text-center">
              <h1 class="text-gold futura mb-2">2018 AWARD CEREMONY</h1>
              <p class="text-white poppins medium">Checkout the information about Volunteer Malaysia Awards 2018 ceremony</p>
          </div>
      </div>
      <div class="row align-items-stretch bg-gray">
          <div class="col-md-6 p-5">
              <div class="row">
                  <div class="col-md-3" style="margin-right:-30px;">
                      <p class="poppins thin text-gold m-0" style="font-size: 3.3em; line-height: 1em">24</p>
                  </div>
                  <div class="col-md-9" >
                      <p class="bold text-white m-0">NOVEMBER</p>
                      <p class="text-white m-0">2018</p>
                  </div>
              </div>
              <div class="row">
                  <div class="col-md-12">
                      <p class="text-white light mt-4" style="font-size: 3.3em;">SATURDAY</p>
                  </div>
              </div>
              <div class="row mt-5">
                  <div class="col-md-12">
                      <p class="text-gold" style="font-size: 2.3em; line-height: 1em">Volunteer Malaysia Awards Ceremony</p>
                      <hr class="hr-gold mt-4" />
                  </div>
              </div>
              <div class="row mt-3">
                  <div class="col-md-2 p-0">
                      <img src="img/icon/mapmarker-01.png" style="width:80px" />
                  </div>
                  <div class="col-md-10 d-flex align-items-center">
                      <p class="text-white">The Rectangle, Rubix, Jalan TPP 1/7, <br/> Taman Perindustrian Puchong, 47100 Puchong, Selangor
                      </p>
                  </div>
              </div>
              <div class="row mt-4">
                  <div class="col-md-12">
                      <a href="https://goo.gl/maps/3z5MqZa6HuT2" target="_blank" class="btn btn-black btn-block">VIEW ON GOOGLE MAP</a>
                  </div>
              </div>
          </div>
          <div class="col-md-6 bg-cover" style="background-image: url('img/awardceremony.jpg');">
          </div>
      </div>
  </div>
</div>


<!--  Awards Categories  -->
<div class="container-fluid bg-black pt-5 pb-5 pl-0 pr-0 my-3">
  <div class="container p-5 bg-black">
      <div class="row">
          <div class="col-md-12 text-center mt-5 mb-5">
              <h1 class="text-gold futura mb-2">AWARD CATEGORIES</h1>
              <p class="text-white poppins medium">List of categories for Volunteer Malaysia Awards 2018</p>
          </div>
      </div>

      <!-- Best volunteer init-->
      <div class="row" style="margin-bottom: -45px">
          <div class="col-md-12 d-flex justify-content-center">
              <div class="bg-gold text-center p-3" style="width:350px">
                  <h5 class="bold m-0" style="font-family: 'futura', sans-serif;">Best Volunteer Initiatives</h5>
              </div>
          </div>
      </div>
      <div class="row mt-4 border-gold p-5 justify-content-center">
          <div class="col-md-4 mt-4">
              <div class="cat-item bg-gray pt-2 d-flex align-items-center flex-column" style="height: 200px;">
                  <img src="img/icon/BestVolunteerInitiative/publicsector-01.png" style="width: 120px" />
                  <p class="text-white mx-3 mt-1 text-center">PUBLIC SERVICE<br/>SECTOR</p>
              </div>
          </div>
          <div class="col-md-4 mt-4">
              <div class="cat-item bg-gray pt-2 d-flex align-items-center flex-column" style="height: 200px">
                  <img src="img/icon/BestVolunteerInitiative/privatesector-01.png" style="width: 120px" />
                  <p class="text-white mx-3 mt-1">PRIVATE<br/>SECTOR</p>
              </div>
          </div>
          <div class="col-md-4 mt-4">
              <div class="cat-item bg-gray pt-2 d-flex align-items-center flex-column" style="height: 200px">
                  <img src="img/icon/BestVolunteerInitiative/schools-01.png" style="width: 120px" />
                  <p class="text-white mx-3 mt-1">SCHOOLS</p>
              </div>
          </div>
          <div class="col-md-4 mt-4">
              <div class="cat-item bg-gray pt-2 d-flex align-items-center flex-column text-center" style="height: 200px">
                  <img src="img/icon/BestVolunteerInitiative/institutionshigher-01.png" style="width: 120px" />
                  <p class="text-white mx-3 mt-1">INSTITUTION OF<br/>HIGHER LEARNING</p>
              </div>
          </div>
          <div class="col-md-4 mt-4">
              <div class="cat-item bg-gray pt-2 d-flex align-items-center flex-column text-center" style="height: 200px">
                  <img src="img/icon/BestVolunteerInitiative/ngo-01.png" style="width: 120px" />
                  <p class="text-white mx-3 mt-1">NGO/NPO/REGISTERED CHARITY</p>
              </div>
          </div>
          <div class="col-md-4 mt-4">
              <div class="cat-item bg-gray pt-2 d-flex align-items-center flex-column" style="height: 200px">
                  <img src="img/icon/BestVolunteerInitiative/media-01.png" style="width: 120px" />
                  <p class="text-white mx-3 mt-1 text-center">MEDIA<br/>ORGANISATIONS</p>
              </div>
          </div>
          <div class="col-md-4 mt-4 mb-3">
              <div class="cat-item bg-gray pt-2 d-flex align-items-center flex-column" style="height: 200px">
                  <img src="img/icon/BestVolunteerInitiative/open-01.png" style="width: 120px" />
                  <p class="text-white mb-0 mt-1">OPEN</p>
                  <p class="text-white mx-3 text-center m-0" style="font-size: 11px; line-height: 1.3em">(Unincorporated organisations e.g. community, voluntary group or association, individuals)</p>
              </div>
          </div>
      </div>

      <!-- Volunteer Malaysia-->
      <div class="row mt-5" style="margin-bottom: -45px">
          <div class="col-md-12 d-flex justify-content-center">
              <div class="bg-gold text-center p-3" style="width:300px">
                  <h5 class="bold m-0" style="font-family: 'futura', sans-serif;">Volunteer Malaysia</h5>
              </div>
          </div>
      </div>
      <div class="row mt-4 border-gold p-5">
          <div class="col-md-4 mt-4">
              <div class="cat-item bg-gray pt-2 d-flex align-items-center flex-column" style="height: 200px">
                  <img src="img/icon/VolunteerMalaysia/superelawan-01.png" style="width: 120px" />
                  <p class="text-white mx-3 mt-1">SUPERELAWAN</p>
                  <p class="text-gold mx-3 text-center m-0 mt-4" style="font-size: 11px; line-height: 1.3em">*Jury selected award. Not open to nominations</p>
              </div>
          </div>
          <div class="col-md-4 mt-4">
              <div class="cat-item bg-gray pt-2 d-flex align-items-center flex-column text-center" style="height: 200px">
                  <img src="img/icon/VolunteerMalaysia/outreachcentre-01.png" style="width: 120px" />
                  <p class="text-white mx-3 mt-1">OUTREACH<br/> CENTER</p>
                  <p class="text-gold mx-3 text-center m-0" style="font-size: 11px; line-height: 1.3em">*Jury selected award. Not open to nominations</p>
              </div>
          </div>
          <div class="col-md-4 mt-4">
              <div class="cat-item bg-gray pt-2 d-flex align-items-center flex-column text-center" style="height: 200px">
                  <img src="img/icon/VolunteerMalaysia/outreach%20officer-01.png" style="width: 120px" />
                  <p class="text-white mx-3 mt-1">OUTREACH<br/>CENTER OFFICER</p>
                  <p class="text-gold mx-3 text-center m-0" style="font-size: 11px; line-height: 1.3em">*Jury selected award. Not open to nominations</p>
              </div>
          </div>
          <div class="col-md-4 mt-4">
              <div class="cat-item bg-gray pt-2 d-flex align-items-center flex-column" style="height: 200px">
                  <img src="img/icon/VolunteerMalaysia/peoplechoice-01.png" style="width: 120px" />
                  <p class="text-white mx-3 mt-1">PEOPLE'S<br/>CHOICE</p>
              </div>
          </div>
          <div class="col-md-4 mt-4">
              <div class="cat-item bg-gray pt-2 d-flex align-items-center flex-column" style="height: 200px">
                  <img src="img/icon/VolunteerMalaysia/Lifetime-01.png" style="width: 120px" />
                  <p class="text-white mx-3 mt-1 text-center">LIFETIME<br/>ACHIEVEMENT</p>
              </div>
          </div>
      </div>


      <!--Radio award -->
      <div class="row mt-5" style="margin-bottom: -45px">
          <div class="col-md-12 d-flex justify-content-center">
              <!-- <div class="bg-gold text-center p-3" style="width:250px">
                  <h5 class="bold m-0" style="font-family: 'futura', sans-serif;">Radio Award</h5>
              </div>
          </div>
      </div>
      <div class="row mt-4 border-gold p-5 justify-content-center">
          <div class="col-md-4 mt-4">
              <div class="cat-item bg-gray pt-2 d-flex align-items-center flex-column" style="height: 200px">
                  <img src="img/icon/radioaward/radio-01.png" style="width: 120px" />
                  <p class="text-white mx-3 mt-1 text-center">BEST<br/>SONG</p>
              </div> -->
          </div>
      </div>
  </div>
</div>



<!--  Awards Categories  -->
<div class="container-fluid bg-black p-5 mt-3 bg-cover" style="background-image: url('img/bottomphoto1.jpg');">
  <div class="container">
      <div class="row p-5 a">
          <div class="col-md-12 d-flex flex-column align-items-center">
              <img src="img/logo/sliderlogo.png" style="width: 380px" />
              <h4 class="futura text-white mt-0 mb-3 text-center text-shadow">JOIN US IN RECOGNISING THE VALUE OF<br/>VOLUNTEERING AND SPIRIT OF VOLUNTEERISM</h4>
              <a href="{{ route('nominees.terms') }}" class="btn btn-black shadow-lg">NOMINATE NOW</a>
          </div>
      </div>
  </div>
</div>

@endsection
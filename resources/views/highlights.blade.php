@extends('layouts.basic.master')

@section('content')
<div class="container-fluid bg-black mt-3">
    <div class="container py-5">
        <div class="row my-4 mt-5 mb-5">
            <div class="col-md-12">
                <h1 class="text-gold futura text-center">HIGHLIGHTS FROM LAST CEREMONY</h1>
            </div>
        </div>
        <div class="row align-items-stretch">
            <div class="col-md-4">
                <div id="video0" class="videoselect d-flex bg-cover justify-content-center align-items-center " style="background-image: url('img/high/vid.png'); height:230px" data-toggle="modal" data-target="#videoModal">
                    <i class="fa fa-play text-white" style="font-size:2em;"></i>
                </div>
                <div class="p-4 bg-gray" style="height:95px">
                    <p class="text-white" style="font-size:1.3em">2016 Volunteer Malaysia Awards</p>
                </div>
            </div>
            <div class="col-md-4">
                <div id="video1" class="videoselect d-flex bg-cover justify-content-center align-items-center " style="background-image: url('img/high/vid2.png'); height:230px" data-toggle="modal" data-target="#videoModal">
                    <i class="fa fa-play text-white" style="font-size:2em;"></i>
                </div>
                <div class="p-4 bg-gray" style="height:95px">
                    <p class="text-white" style="font-size:1.3em">VMA Ceremony Night</p>
                </div>
            </div>
            <div class="col-md-4">
                <div id="video2" class="videoselect d-flex bg-cover justify-content-center align-items-center " style="background-image: url('img/high/vid3.png'); height:230px" data-toggle="modal" data-target="#videoModal">
                    <i class="fa fa-play text-white" style="font-size:2em;"></i>
                </div>
                <div class="p-4 bg-gray" style="height:95px">
                    <p class="text-white" style="font-size:1.3em">2016 Volunteer Malaysia Awards</p>
                </div>
            </div>
        </div>
        <div class="row my-4">
            <div class="col-md-12">
                <hr class="hr-gold" />
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <p class="text-white">Check out the photo gallery from 2016 Volunteer Malaysia Award Ceremony.</p>
            </div>
        </div>
        <div class="row">
            <div id="slide0" class="imageselect col-md-3" data-toggle="modal" data-target="#exampleModal">
                <div class="bg-cover" style="background-image: url('img/highlights/1.jpg'); height: 200px;"></div>
            </div>
            <div id="slide1" class="imageselect col-md-3" data-toggle="modal" data-target="#exampleModal">
                <div class="bg-cover" style="background-image: url('img/highlights/2.jpg'); height: 200px;"></div>
            </div>
            <div id="slide2" class="imageselect col-md-3" data-toggle="modal" data-target="#exampleModal">
                <div class="bg-cover" style="background-image: url('img/highlights/3.jpg'); height: 200px;"></div>
            </div>
            <div id="slide3" class="imageselect col-md-3" data-toggle="modal" data-target="#exampleModal">
                <div class="bg-cover" style="background-image: url('img/highlights/4.jpg'); height: 200px;"></div>
            </div>
            <div id="slide4" class="imageselect col-md-3" data-toggle="modal" data-target="#exampleModal">
                <div class="bg-cover" style="background-image: url('img/highlights/5.jpg'); height: 200px;"></div>
            </div>
            <div id="slide5" class="imageselect col-md-3" data-toggle="modal" data-target="#exampleModal">
                <div class="bg-cover" style="background-image: url('img/highlights/6.jpg'); height: 200px;"></div>
            </div>
            <div id="slide6" class="imageselect col-md-3" data-toggle="modal" data-target="#exampleModal">
                <div class="bg-cover" style="background-image: url('img/highlights/7.jpg'); height: 200px;"></div>
            </div>
            <div id="slide7" class="imageselect col-md-3" data-toggle="modal" data-target="#exampleModal">
                <div class="bg-cover" style="background-image: url('img/highlights/8.jpg'); height: 200px;"></div>
            </div>
            <div id="slide8" class="imageselect col-md-3" data-toggle="modal" data-target="#exampleModal">
                <div class="bg-cover" style="background-image: url('img/highlights/9.jpg'); height: 200px;"></div>
            </div>
            <div id="slide9" class="imageselect col-md-3" data-toggle="modal" data-target="#exampleModal">
                <div class="bg-cover" style="background-image: url('img/highlights/10.jpg'); height: 200px;"></div>
            </div>
            <div id="slide10" class="imageselect col-md-3" data-toggle="modal" data-target="#exampleModal">
                <div class="bg-cover" style="background-image: url('img/highlights/11.jpg'); height: 200px;"></div>
            </div>
            <div id="slide11" class="imageselect col-md-3" data-toggle="modal" data-target="#exampleModal">
                <div class="bg-cover" style="background-image: url('img/highlights/12.jpg'); height: 200px;"></div>
            </div>
            <div id="slide12" class="imageselect col-md-3" data-toggle="modal" data-target="#exampleModal">
                <div class="bg-cover" style="background-image: url('img/highlights/13.jpg'); height: 200px;"></div>
            </div>
            <div id="slide13" class="imageselect col-md-3" data-toggle="modal" data-target="#exampleModal">
                <div class="bg-cover" style="background-image: url('img/highlights/14.jpg'); height: 200px;"></div>
            </div>
            <div id="slide14" class="imageselect col-md-3" data-toggle="modal" data-target="#exampleModal">
                <div class="bg-cover" style="background-image: url('img/highlights/15.jpg'); height: 200px;"></div>
            </div>
            <div id="slide15" class="imageselect col-md-3" data-toggle="modal" data-target="#exampleModal">
                <div class="bg-cover" style="background-image: url('img/highlights/16.jpg'); height: 200px;"></div>
            </div>
            <div id="slide16" class="imageselect col-md-3" data-toggle="modal" data-target="#exampleModal">
                <div class="bg-cover" style="background-image: url('img/highlights/17.jpg'); height: 200px;"></div>
            </div>
            <div id="slide17" class="imageselect col-md-3" data-toggle="modal" data-target="#exampleModal">
                <div class="bg-cover" style="background-image: url('img/highlights/18.jpg'); height: 200px;"></div>
            </div>
            <div id="slide18" class="imageselect col-md-3" data-toggle="modal" data-target="#exampleModal">
                <div class="bg-cover" style="background-image: url('img/highlights/19.jpg'); height: 200px;"></div>
            </div>
            <div id="slide19" class="imageselect col-md-3" data-toggle="modal" data-target="#exampleModal">
                <div class="bg-cover" style="background-image: url('img/highlights/20.jpg'); height: 200px;"></div>
            </div>
        </div>
    </div>
</div>

<!-- Modal Image -->
<div class="modal p-0 fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document" style="max-width: 900px">
        <div class="modal-content p-0" style="max-width: 900px">
            <div class="modal-body p-0">
                <div id="carouselimage" class="carousel slide">
                    <ol class="carousel-indicators">
                        <li data-target="#carouselimage" data-slide-to="0"></li>
                        <li data-target="#carouselimage" data-slide-to="1"></li>
                        <li data-target="#carouselimage" data-slide-to="2"></li>
                        <li data-target="#carouselimage" data-slide-to="3"></li>
                        <li data-target="#carouselimage" data-slide-to="4"></li>
                        <li data-target="#carouselimage" data-slide-to="5"></li>
                        <li data-target="#carouselimage" data-slide-to="6"></li>
                        <li data-target="#carouselimage" data-slide-to="7"></li>
                        <li data-target="#carouselimage" data-slide-to="8"></li>
                        <li data-target="#carouselimage" data-slide-to="9"></li>
                        <li data-target="#carouselimage" data-slide-to="10"></li>
                        <li data-target="#carouselimage" data-slide-to="11"></li>
                        <li data-target="#carouselimage" data-slide-to="12"></li>
                        <li data-target="#carouselimage" data-slide-to="13"></li>
                        <li data-target="#carouselimage" data-slide-to="14"></li>
                        <li data-target="#carouselimage" data-slide-to="15"></li>
                        <li data-target="#carouselimage" data-slide-to="16"></li>
                        <li data-target="#carouselimage" data-slide-to="17"></li>
                        <li data-target="#carouselimage" data-slide-to="18"></li>
                        <li data-target="#carouselimage" data-slide-to="19"></li>
                    </ol>
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <img class="d-block w-100" src="img/highlights/1.jpg" alt="First slide">
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="img/highlights/2.jpg" alt="Second slide">
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="img/highlights/3.jpg" alt="Third slide">
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="img/highlights/4.jpg" alt="Fourth slide">
                        </div>
                         <div class="carousel-item">
                            <img class="d-block w-100" src="img/highlights/5.jpg" alt="Fifth slide">
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="img/highlights/6.jpg" alt="Sixth slide">
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="img/highlights/7.jpg" alt="Seventh slide">
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="img/highlights/8.jpg" alt="Eight slide">
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="img/highlights/9.jpg" alt="Nine slide">
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="img/highlights/10.jpg" alt="Ten slide">
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="img/highlights/11.jpg" alt="Eleven slide">
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="img/highlights/12.jpg" alt="Twelve slide">
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="img/highlights/13.jpg" alt="Thirteen slide">
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="img/highlights/14.jpg" alt="Fourteen slide">
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="img/highlights/15.jpg" alt="Fifteen slide">
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="img/highlights/16.jpg" alt="Sixteen slide">
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="img/highlights/17.jpg" alt="Seventeen slide">
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="img/highlights/18.jpg" alt="Eighteen slide">
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="img/highlights/19.jpg" alt="Nineteen slide">
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="img/highlights/20.jpg" alt="Twenty slide">
                        </div>
                    </div>
                    <a class="carousel-control-prev" href="#carouselimage" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselimage" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Video Image -->
<div class="modal p-0 fade" id="videoModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document" style="max-width: 900px">
        <div class="modal-content p-0" style="max-width: 900px">
            <div class="modal-body p-0">
                <div id="carouselvideo" class="carousel slide">
                    <ol class="carousel-indicators">
                        <li data-target="#carouselvideo" data-slide-to="0"></li>
                        <li data-target="#carouselvideo" data-slide-to="1"></li>
                        <li data-target="#carouselvideo" data-slide-to="2"></li>
                    </ol>
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <iframe style="width: 100%; height: 560px; margin-bottom:-10px;" src="https://www.youtube.com/embed/NRJuULzEsRI?rel=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                        </div>
                        <div class="carousel-item">
                            <iframe style="width: 100%; height: 560px; margin-bottom:-10px;" src="https://www.youtube.com/embed/C2gAHg-sQ9E?rel=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                        </div>
                        <div class="carousel-item">
                            <iframe style="width: 100%; height: 560px; margin-bottom:-10px;" src="https://www.youtube.com/embed/y7ZB31tSQfw?rel=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                        </div>
                    </div>
                    <a class="carousel-control-prev" href="#carouselvideo" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselvideo" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js') 
@parent
<script>
$(document).ready(function(){
    $("#slide0").click(function() {
        $("#carouselimage").carousel(0);
    });
    $("#slide1").click(function() {
        $("#carouselimage").carousel(1);
    });
    $("#slide2").click(function() {
        $("#carouselimage").carousel(2);
    });
    $("#slide3").click(function() {
        $("#carouselimage").carousel(3);
    });
    $("#slide4").click(function() {
        $("#carouselimage").carousel(4);
    });
    $("#slide5").click(function() {
        $("#carouselimage").carousel(5);
    });
    $("#slide6").click(function() {
        $("#carouselimage").carousel(6);
    });
    $("#slide7").click(function() {
        $("#carouselimage").carousel(7);
    });
    $("#slide8").click(function() {
        $("#carouselimage").carousel(8);
    });
    $("#slide9").click(function() {
        $("#carouselimage").carousel(9);
    });
    $("#slide10").click(function() {
        $("#carouselimage").carousel(10);
    });
    $("#slide11").click(function() {
        $("#carouselimage").carousel(11);
    });
    $("#slide12").click(function() {
        $("#carouselimage").carousel(12);
    });
    $("#slide13").click(function() {
        $("#carouselimage").carousel(13);
    });
    $("#slide14").click(function() {
        $("#carouselimage").carousel(14);
    });
    $("#slide15").click(function() {
        $("#carouselimage").carousel(15);
    });
    $("#slide16").click(function() {
        $("#carouselimage").carousel(16);
    });
    $("#slide17").click(function() {
        $("#carouselimage").carousel(17);
    });
    $("#slide18").click(function() {
        $("#carouselimage").carousel(18);
    });
    $("#slide19").click(function() {
        $("#carouselimage").carousel(19);
    });
});

</script>

<script>
$(document).ready(function(){
    
    $("#video0").click(function() {
        $("#carouselvideo").carousel(0);
    });
    $("#video1").click(function() {
        $("#carouselvideo").carousel(1);
    });
    $("#video2").click(function() {
        $("#carouselvideo").carousel(2);
    });
});
</script>

@endsection
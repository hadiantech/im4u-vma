@extends('layouts.basic.master')

@section('content')
<div class="container-fluid bg-black pt-3 pb-5 pl-0 pr-0 my-3">
    <div class="container p-5 bg-black">
        <div class="row">
            <div class="col-md-12 text-center mt-4 mb-4">
                <h1 class="text-gold futura mb-2">AWARD CATEGORIES</h1>
                <p class="text-white poppins medium">Description of categories for Volunteer Malaysia Awards 2018</p>
            </div>
        </div>
  
        <!-- Best volunteer init-->
        <div class="row" style="margin-bottom: -45px">
            <div class="col-md-12 d-flex justify-content-center">
                <div class="bg-gold text-center p-3" style="width:350px">
                    <h5 class="bold m-0" style="font-family: 'futura', sans-serif;">Best Volunteer Initiatives</h5>
                </div>
            </div>
        </div>

        <div class="row mt-4 border-gold pt-5 pb-5 pl-1 pr-1 mb-5 justify-content-center">
            <div class="col-md-4 p-1">
                <div class="cat-item bg-gray pt-2 d-flex align-items-center flex-column" style="height: 200px;">
                    <img src="{{ asset('img/icon/BestVolunteerInitiative/publicsector-01.png') }}" style="width: 120px" />
                    <p class="text-white mx-3 mt-1 text-center">PUBLIC SERVICE<br/>SECTOR</p>
                </div>
            </div>
            <div class="col-md-6 p-1">
                <div class="cat-item bg-gray p-5 d-flex align-items-center justify-content-left" style="height: 200px;">
                    <p class="text-white mx-3 mt-1 text-left" style="font-size: 12px;">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident.</p>
                </div>
            </div>
            
            <div class="col-md-4 p-1 pt-3">
                <div class="cat-item bg-gray pt-2 d-flex align-items-center flex-column" style="height: 200px;">
                    <img src="{{ asset('img/icon/BestVolunteerInitiative/privatesector-01.png') }}" style="width: 120px" />
                    <p class="text-white mx-3 mt-1 text-center">PRIVATE<br/>SECTOR</p>
                </div>
            </div>
            <div class="col-md-6 p-1 pt-3">
                <div class="cat-item bg-gray p-5 d-flex align-items-center justify-content-left" style="height: 200px;">
                    <p class="text-white mx-3 mt-1 text-left" style="font-size: 12px;">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident.</p>
                </div>
            </div>

            <div class="col-md-4 p-1 pt-3">
                <div class="cat-item bg-gray pt-2 d-flex align-items-center flex-column" style="height: 200px;">
                    <img src="{{ asset('img/icon/BestVolunteerInitiative/schools-01.png') }}" style="width: 120px" />
                    <p class="text-white mx-3 mt-1 text-center">SCHOOLS</p>
                </div>
            </div>
            <div class="col-md-6 p-1 pt-3">
                <div class="cat-item bg-gray p-5 d-flex align-items-center justify-content-left" style="height: 200px;">
                    <p class="text-white mx-3 mt-1 text-left" style="font-size: 12px;">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident.</p>
                </div>
            </div>

            <div class="col-md-4 p-1 pt-3">
                <div class="cat-item bg-gray pt-2 d-flex align-items-center flex-column" style="height: 200px;">
                    <img src="{{ asset('img/icon/BestVolunteerInitiative/institutionshigher-01.png') }}" style="width: 120px" />
                    <p class="text-white mx-3 mt-1 text-center">INSTITUTION OF<br/>HIGHER LEARNING</p>
                </div>
            </div>
            <div class="col-md-6 p-1 pt-3">
                <div class="cat-item bg-gray p-5 d-flex align-items-center justify-content-left" style="height: 200px;">
                    <p class="text-white mx-3 mt-1 text-left" style="font-size: 12px;">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident.</p>
                </div>
            </div>

            <div class="col-md-4 p-1 pt-3">
                <div class="cat-item bg-gray pt-2 d-flex align-items-center flex-column" style="height: 200px;">
                    <img src="{{ asset('img/icon/BestVolunteerInitiative/ngo-01.png') }}" style="width: 120px" />
                    <p class="text-white mx-3 mt-1 text-center">NGO/NPO/<br/>REGISTERED CHARITY</p>
                </div>
            </div>
            <div class="col-md-6 p-1 pt-3">
                <div class="cat-item bg-gray p-5 d-flex align-items-center justify-content-left" style="height: 200px;">
                    <p class="text-white mx-3 mt-1 text-left" style="font-size: 12px;">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident.</p>
                </div>
            </div>

            <div class="col-md-4 p-1 pt-3">
                <div class="cat-item bg-gray pt-2 d-flex align-items-center flex-column" style="height: 200px;">
                    <img src="{{ asset('img/icon/BestVolunteerInitiative/media-01.png') }}" style="width: 120px" />
                    <p class="text-white mx-3 mt-1 text-center">MEDIA<br/>ORGANISATIONS</p>
                </div>
            </div>
            <div class="col-md-6 p-1 pt-3">
                <div class="cat-item bg-gray p-5 d-flex align-items-center justify-content-left" style="height: 200px;">
                    <p class="text-white mx-3 mt-1 text-left" style="font-size: 12px;">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident.</p>
                </div>
            </div>

            <div class="col-md-4 p-1 pt-3">
                <div class="cat-item bg-gray pt-2 d-flex align-items-center flex-column" style="height: 200px;">
                    <img src="{{ asset('img/icon/BestVolunteerInitiative/open-01.png') }}" style="width: 120px" />
                    <p class="text-white mb-0 mt-1">OPEN</p>
                    <p class="text-white mx-3 text-center m-0" style="font-size: 11px; line-height: 1.3em">(Unincorporated organisations e.g. community, <br> voluntary group or association, individuals)</p>
                </div>
            </div>
            <div class="col-md-6 p-1 pt-3">
                <div class="cat-item bg-gray p-5 d-flex align-items-center justify-content-left" style="height: 200px;">
                    <p class="text-white mx-3 mt-1 text-left" style="font-size: 12px;">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident.</p>
                </div>
            </div>

        </div>

        <!-- Volunteer Malaysia-->
        <div class="row" style="margin-bottom: -45px">
            <div class="col-md-12 d-flex justify-content-center">
                <div class="bg-gold text-center p-3" style="width:350px">
                    <h5 class="bold m-0" style="font-family: 'futura', sans-serif;">Volunteer Malaysia</h5>
                </div>
            </div>
        </div>

        <div class="row mt-4 border-gold pt-5 pb-5 pl-1 pr-1 justify-content-center">
            <div class="col-md-4 p-1">
                <div class="cat-item bg-gray pt-2 d-flex align-items-center flex-column" style="height: 200px;">
                    <img src="{{ asset('img/icon/VolunteerMalaysia/Lifetime-01.png') }}" style="width: 120px" />
                    <p class="text-white mx-3 mt-1 text-center">LIFETIME<br/>ACHIEVEMENT</p>
                </div>
            </div>
            <div class="col-md-6 p-1">
                <div class="cat-item bg-gray p-5 d-flex align-items-center justify-content-left" style="height: 200px;">
                    <p class="text-white mx-3 mt-1 text-left" style="font-size: 12px;">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident.</p>
                </div>
            </div>
            
            <div class="col-md-4 p-1 pt-3">
                <div class="cat-item bg-gray pt-2 d-flex align-items-center flex-column" style="height: 200px;">
                    <img src="{{ asset('img/icon/VolunteerMalaysia/superelawan-01.png') }}" style="width: 120px" />
                    <p class="text-white mx-3 mt-1 text-center">SUPERELAWAN</p>
                    <p class="text-gold mx-3 text-center m-0 mt-4" style="font-size: 11px; line-height: 1.3em">*Jury selected award. Not open to nominations</p>
                </div>
            </div>
            <div class="col-md-6 p-1 pt-3">
                <div class="cat-item bg-gray p-5 d-flex align-items-center justify-content-left" style="height: 200px;">
                    <p class="text-white mx-3 mt-1 text-left" style="font-size: 12px;">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident.</p>
                </div>
            </div>

            <div class="col-md-4 p-1 pt-3">
                <div class="cat-item bg-gray pt-2 d-flex align-items-center flex-column" style="height: 200px;">
                    <img src="{{ asset('img/icon/VolunteerMalaysia/peoplechoice-01.png') }}" style="width: 120px" />
                    <p class="text-white mx-3 mt-1 text-center">PEOPLE'S <br/>CHOICE</p>
                </div>
            </div>
            <div class="col-md-6 p-1 pt-3">
                <div class="cat-item bg-gray p-5 d-flex align-items-center justify-content-left" style="height: 200px;">
                    <p class="text-white mx-3 mt-1 text-left" style="font-size: 12px;">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident.</p>
                </div>
            </div>

            <div class="col-md-4 p-1 pt-3">
                <div class="cat-item bg-gray pt-2 d-flex align-items-center flex-column" style="height: 200px;">
                    <img src="{{ asset('img/icon/VolunteerMalaysia/outreachcentre-01.png') }}" style="width: 120px" />
                    <p class="text-white mx-3 mt-1 text-center">OUTREACH<br/>CENTER</p>
                    <p class="text-gold mx-3 text-center m-0" style="font-size: 11px; line-height: 1.3em">*Jury selected award. Not open to nominations</p>
                </div>
            </div>
            <div class="col-md-6 p-1 pt-3">
                <div class="cat-item bg-gray p-5 d-flex align-items-center justify-content-left" style="height: 200px;">
                    <p class="text-white mx-3 mt-1 text-left" style="font-size: 12px;">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident.</p>
                </div>
            </div>

            <div class="col-md-4 p-1 pt-3">
                <div class="cat-item bg-gray pt-2 d-flex align-items-center flex-column" style="height: 200px;">
                    <img src="{{ asset('img/icon/VolunteerMalaysia/outreach%20officer-01.png') }}" style="width: 120px" />
                    <p class="text-white mx-3 mt-1 text-center">OUTREACH<br/>CENTER OFFICER</p>
                    <p class="text-gold mx-3 text-center m-0" style="font-size: 11px; line-height: 1.3em">*Jury selected award. Not open to nominations</p>
                </div>
            </div>
            <div class="col-md-6 p-1 pt-3">
                <div class="cat-item bg-gray p-5 d-flex align-items-center justify-content-left" style="height: 200px;">
                    <p class="text-white mx-3 mt-1 text-left" style="font-size: 12px;">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident.</p>
                </div>
            </div>
        </div>
        

    </div>
</div>


@endsection
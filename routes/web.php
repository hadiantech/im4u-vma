<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');
Route::get('/home',  'HomeController@index');
Route::get('/about',  'HomeController@about')->name('about');
Route::get('/sponsorship',  'HomeController@sponsorship')->name('sponsorship');
Route::get('/winners',  'HomeController@winners')->name('winners');
Route::get('/highlights',  'HomeController@highlights')->name('highlights');
Route::get('/sponsorship',  'HomeController@sponsorship')->name('sponsorship');
Route::get('/contact',  'HomeController@contact')->name('contact');
Route::get('locale/{locale}', function ($locale) {
    \Session::put('locale', $locale);
    return redirect()->back();
})->name('locale');

Route::get('/categories/list',  'HomeController@listcategories')->name('listcategories');

Auth::routes();

Route::resource('contacts', 'ContactController')->only([
    'store'
]);

Route::get('nominees/terms', 'NomineeController@term_condition')->name('nominees.terms');
Route::resource('nominees', 'NomineeController')->only([
    'create', 'show', 'store'
]);

Route::group( ['middleware' => ['auth']], function() {

   


    Route::get('/dashboard',  'HomeController@dashboard')->name('dashboard');
    
    Route::get('/profile',  'HomeController@profile')->name('profile');
    Route::get('/profile/edit',  'HomeController@profile_edit')->name('profile.edit');
    Route::get('/profile/password',  'HomeController@profile_edit_pw')->name('profile.edit_pw');
    Route::put('/profile/update',  'HomeController@profile_update')->name('profile.update');
    Route::put('/profile/password/update',  'HomeController@profile_pw_update')->name('profile.pw_update');
    
    Route::resource('users', 'UserController');
    Route::get('/users/password/{id}/edit', 'UserController@edit_pw')->name('users.edit_pw');    
    Route::put('/users/password/{id}', 'UserController@update_pw')->name('users.update_pw');    

    Route::resource('roles', 'RoleController');
    Route::resource('categories', 'CategoryController');
    // Route::resource('posts', 'PostController');

    Route::resource('nominees', 'NomineeController')->except([
        'create', 'show', 'store'
    ]);

    Route::resource('contacts', 'ContactController')->except([
        'store'
    ]);

    //datatables
    Route::get('datatables/getUser', 'DatatablesController@getUser');
    Route::get('datatables/getNominee', 'DatatablesController@getNominee');


    //upload 
    Route::post('upload/image', 'UploadController@upload_image')->name('upload.image');
});
